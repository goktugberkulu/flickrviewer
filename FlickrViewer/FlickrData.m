//
//  FlickrData.m
//  FlickrViewer
//
//  Created by Goktug on 23/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "FlickrData.h"

@implementation FlickrData

-(instancetype)initWithParameters:(NSMutableArray *)photos currentPage:(NSNumber *)currentPage totalPage:(NSNumber *)totalPage perPage:(NSNumber *)perPage {
    if (self = [super init]) {
        self.photos = [[NSMutableArray alloc] init];
        self.photos = photos;
        self.currentPage = currentPage;
        self.totalPage = totalPage;
        self.perPage = perPage;
    }
    return self;
}

@end
