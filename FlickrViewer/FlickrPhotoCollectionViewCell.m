//
//  FlickrPhotoCollectionViewCell.m
//  FlickrViewer
//
//  Created by Goktug on 23/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "FlickrPhotoCollectionViewCell.h"

@implementation FlickrPhotoCollectionViewCell

- (UIImageView *) imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        [self.contentView addSubview:_imageView];
    }
    return _imageView;
}

@end
