//
//  FlickrPhotoDetailViewController.m
//  FlickrViewer
//
//  Created by Goktug on 23/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "FlickrPhotoDetailViewController.h"

@interface FlickrPhotoDetailViewController ()

@end

@implementation FlickrPhotoDetailViewController

- (instancetype)initWithData:(NSMutableArray *)photos index:(int)index background:(UIImage *)background {
    self = [super init];
    if (self) {
        self.photos = photos;
        self.photoIndex = index;
        self.background = background;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configurePageViewController];
}

- (void) configurePageViewController {
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:[NSDictionary
                                dictionaryWithObject:[NSNumber numberWithFloat:0.0f] forKey:UIPageViewControllerOptionInterPageSpacingKey]];
    self.pageViewController.dataSource = self;
    FlickrPhotoPageContentViewController *startingViewController = [self viewControllerAtIndex:self.photoIndex];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    NSUInteger index = ((FlickrPhotoPageContentViewController*) viewController).photoIndex;
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    NSUInteger index = ((FlickrPhotoPageContentViewController*) viewController).photoIndex;
    if (index == NSNotFound) {
        return nil;
    }
    index++;
    if (index == [self.photos count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (FlickrPhotoPageContentViewController *)viewControllerAtIndex:(NSUInteger)index {
    if (([self.photos count] == 0) || (index >= [self.photos count])) {
        return nil;
    }
    FlickrPhotoPageContentViewController *pageContentViewController = [[FlickrPhotoPageContentViewController alloc]initWithData:self.photos index:(int)index background:self.background];
    return pageContentViewController;
}

@end
