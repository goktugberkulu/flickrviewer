//
//  FlickrRequest.m
//  FlickrViewer
//
//  Created by Goktug on 22/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "FlickrRequest.h"
#import "AFNetworking.h"

static NSString *const FlickrAPIKey = @"7e58c6d0fa5e80791887b2f38b312225";

@interface FlickrRequest()

@end

@implementation FlickrRequest

 + (void) findPhotosWithParamaters:(NSArray *)parameters completionHandler:(void (^)(FlickrData *data))callback {
     FlickrData *flickr = [[FlickrData alloc] initWithParameters:[NSMutableArray new] currentPage:0 totalPage:0 perPage:0];
     NSMutableString *urlString = [NSMutableString stringWithFormat:@"https://api.flickr.com/services/rest/?&api_key=%@&format=json&nojsoncallback=1", FlickrAPIKey];
     for (int i = 0; i < [parameters count]; i++){
         if (i % 2 == 0) {
             [urlString appendString:[NSMutableString stringWithFormat:@"&%@=", [parameters objectAtIndex:i]]];
         } else {
             [urlString appendString:[NSMutableString stringWithFormat:@"%@", [parameters objectAtIndex:i]]];
         }
     }
     
     NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
     AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
     
     NSURL *url = [NSURL URLWithString:urlString];
     [manager initWithBaseURL:url];
     
     manager.responseSerializer = [AFJSONResponseSerializer serializer];
     [manager GET:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
    
        NSLog(@"response object: %@", responseObject);
        NSDictionary *json = (NSDictionary *) responseObject;
        flickr.currentPage = (NSNumber *)[[json objectForKey:@"photos"] objectForKey:@"page"];
        flickr.totalPage = (NSNumber *)[[json objectForKey:@"photos"] objectForKey:@"pages"];
        flickr.perPage = (NSNumber *)[[json objectForKey:@"photos"] objectForKey:@"perpage"];
        NSArray *photos = [[json objectForKey:@"photos"] objectForKey:@"photo"];
             
        for (NSDictionary *photo in photos) {
            NSString *title = [photo objectForKey:@"title"];
                 
            NSURL *photoURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://farm%@.static.flickr.com/%@/%@_%@_b.jpg",
                                                         [photo objectForKey:@"farm"], [photo objectForKey:@"server"],
                                                         [photo objectForKey:@"id"], [photo objectForKey:@"secret"]]];
            FlickrPhoto *flickrPhoto = [[FlickrPhoto alloc] initWithParamaters:title url:photoURL];
            [flickr.photos addObject:flickrPhoto];
        }
             
        callback(flickr);
             
         
     } failure:^(NSURLSessionDataTask *task, NSError *error) {
         NSLog(@"%@", error);
     }];
 }

@end
