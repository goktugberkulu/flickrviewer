//
//  FlickrPhotoPageContentViewController.m
//  FlickrViewer
//
//  Created by Goktug on 29/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "FlickrPhotoPageContentViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface FlickrPhotoPageContentViewController ()


@end

@implementation FlickrPhotoPageContentViewController

- (instancetype)initWithData:(NSMutableArray *)photos index:(int)photoIndex background:(UIImage *)background{
    if (self = [super init]) {
        self.photos = photos;
        self.photoIndex = photoIndex;
        self.background = background;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    self.backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview: self.backgroundImageView];

    
    [self configureScrollView];
    
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 20 + 1.25 * self.view.frame.size.height, self.scrollView.frame.size.width, self.view.frame.size.height/2)];
    
    
    self.scrollView.contentSize = CGSizeMake(0, 3 * self.view.frame.size.height);
    self.detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 2 * self.view.frame.size.height - 50, self.view.frame.size.width, 50)];
    self.detailLabel.textColor = [UIColor whiteColor];
    self.detailLabel.backgroundColor = [UIColor grayColor];
    self.detailLabel.adjustsFontSizeToFitWidth = YES;
    self.detailLabel.numberOfLines = 0;
    [self.detailLabel setAlpha:0.0f];
    self.detailLabel.textAlignment = NSTextAlignmentCenter;
    
    
    self.topDetailLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 20 + self.navigationController.navigationBar.frame.size.height + self.view.frame.size.height, self.view.frame.size.width, 50)];
    self.topDetailLabel.backgroundColor = [UIColor grayColor];
    [self.topDetailLabel setAlpha:0.0f];
    
    
    
    [self.scrollView addSubview:self.detailLabel];
    [self.scrollView addSubview:self.topDetailLabel];
    [self downloadAndDisplayNewImage];
    [self.scrollView addSubview:self.imageView];
    
    CGRect scrollBounds = self.scrollView.bounds;
    scrollBounds.origin = CGPointMake(0, self.view.frame.size.height);
    self.scrollView.bounds = scrollBounds;
    
    

    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
    [self.scrollView setShowsVerticalScrollIndicator:NO];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    
    
    //fade in
    [UIView animateWithDuration:1.0f animations:^{
        if (self.detailLabel.alpha == 1.0) {
            [self.detailLabel setAlpha:0.0f];
            [self.topDetailLabel setAlpha:0.0f];
        } else if (self.detailLabel.alpha == 0.0) {
            [self.detailLabel setAlpha:1.0f];
            [self.topDetailLabel setAlpha:1.0f];
        }
    } completion:^(BOOL finished) {
        
        //fade out
      /*  [UIView animateWithDuration:2.0f animations:^{
            
            
        } completion:nil];*/
        
    }];
    
    
    
    NSLog(@"tapped");
}

- (void) configureScrollView {
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.scrollView.userInteractionEnabled=YES;
    self.scrollView.minimumZoomScale = 1.0;
    self.scrollView.maximumZoomScale = 4.0;
    self.scrollView.delegate = self;
    self.scrollView.scrollEnabled = YES;
    [self.view addSubview:self.scrollView];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
   
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if(self.scrollView.zoomScale == 1.0) {
        self.view.backgroundColor = [UIColor clearColor];
        self.backgroundImageView.alpha = 1.0;
        self.backgroundImageView.image = self.background;
    } else {
        NSLog(@"%f", self.scrollView.zoomScale);
        if (self.scrollView.zoomScale == 1.0) {
        }
    }
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {

        if (!decelerate && self.scrollView.zoomScale == 1.0) {
            [self navigateBackWhenScroll];
            
        } else {
            NSLog(@"%f", self.scrollView.zoomScale);
            if (self.scrollView.zoomScale == 1.0) {
                
            }
        }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    

        if(self.scrollView.zoomScale == 1.0) {
            [self navigateBackWhenScroll];
        } else {
            NSLog(@"%f", self.scrollView.zoomScale);
            if (self.scrollView.zoomScale == 1.0) {
                
            }
            
        }
}

- (void) navigateBackWhenScroll {
    
    if (self.scrollView.contentOffset.y >  1.5 * self.view.frame.size.height ||
        self.scrollView.contentOffset.y < self.view.frame.size.height / 2) {
        [UIView animateWithDuration:0.8 delay:0.0 options:0 animations:^{
            self.view.transform = CGAffineTransformScale(self.view.transform, 0.01, 0.01);
        } completion:^(BOOL completed){
            [self.navigationController popViewControllerAnimated:YES];
        }];
    } else {
        self.view.backgroundColor = [UIColor blackColor];
        self.backgroundImageView.image = nil;
        CGRect scrollBounds = self.scrollView.bounds;
        scrollBounds.origin = CGPointMake(0, self.view.frame.size.height);
        self.scrollView.bounds = scrollBounds;
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    
    UIView *subView=[scrollView.subviews objectAtIndex:0];
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5 + offsetY);
    
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {

}

-(void)downloadAndDisplayNewImage {
    [self.imageView sd_setImageWithURL: [[self.photos objectAtIndex:self.photoIndex]url]
                      placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    self.imageView.userInteractionEnabled=YES;
    self.imageView.contentMode = UIViewContentModeScaleToFill;
    [self.scrollView addSubview:self.imageView];
    self.detailLabel.text = [[self.photos objectAtIndex:self.photoIndex] title];
}

@end
