//
//  ViewController.h
//  FlickrViewer
//
//  Created by Goktug on 22/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlickrRequest.h"
#import "FlickrData.h"
#import "FlickrPhotoDetailViewController.h"
#import "FlickrPhotoCollectionViewCell.h"

@interface ViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate, UISearchDisplayDelegate>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) FlickrData *flickrData;
@property (copy, nonatomic) NSString *tag;
@property (copy, nonatomic) NSString *requestType;
@property (copy, nonatomic) NSString *latitude;
@property (copy, nonatomic) NSString *longitude;

- (instancetype) initWithTag:(NSString *)tag;

- (instancetype) initWithPage:(NSString *)page;

- (instancetype) initWithLocation:(NSString *)latitude longitude:(NSString *)longitude;

-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer;

@end

