//
//  FlickrPhotoPageContentViewController.h
//  FlickrViewer
//
//  Created by Goktug on 29/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrPhotoPageContentViewController : UIViewController <UIScrollViewDelegate>

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIImageView *backgroundImageView;
@property (assign, nonatomic) NSUInteger photoIndex;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) NSMutableArray *photos;
@property (strong, nonatomic) UIImage *background;
@property (strong, nonatomic) UILabel *detailLabel;
@property (strong, nonatomic) UILabel *topDetailLabel;


- (instancetype) initWithData:(NSMutableArray *)photos index:(int)photoIndex background:(UIImage *)background;

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer;

@end
