//
//  MainViewController.h
//  FlickrViewer
//
//  Created by Goktug on 24/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MainViewController : UIViewController <UISearchBarDelegate, UISearchDisplayDelegate>

@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) UIButton *locationBasedPhotoSearchButton;
@property (strong, nonatomic) UIButton *getRecentButton;
@property (strong, nonatomic) CLLocationManager *locationManager;

@end
