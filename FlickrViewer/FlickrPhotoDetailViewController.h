//
//  FlickrPhotoDetailViewController.h
//  FlickrViewer
//
//  Created by Goktug on 23/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlickrPhotoPageContentViewController.h"

@interface FlickrPhotoDetailViewController : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) NSMutableArray *photos;
@property (assign) int photoIndex;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) UIImage *background;

- (instancetype)initWithData:(NSMutableArray *)photos index:(int)index background:(UIImage *)background;

@end
