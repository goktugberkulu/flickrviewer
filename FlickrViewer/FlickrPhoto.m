//
//  FlickrPhoto.m
//  FlickrViewer
//
//  Created by Goktug on 23/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "FlickrPhoto.h"

@implementation FlickrPhoto

- (instancetype)initWithParamaters:(NSString *)title url:(NSURL *)url {
    if (self = [super init]) {
        self.title = title;
        self.url = url;
    }
    return self;
}

@end
