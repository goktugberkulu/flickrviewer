//
//  ViewController.m
//  FlickrViewer
//
//  Created by Goktug on 22/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "ViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ViewController ()

@end

@implementation ViewController

- (instancetype) initWithTag:(NSString *)tag {
    self = [super init];
    if (self) {
        self.tag = tag;
        self.requestType = @"tag";
        self.title = [NSString stringWithFormat:@"#%@", tag];
    }
    return self;
}

- (instancetype) initWithPage:(NSString *)page {
    self = [super init];
    if (self) {
        self.requestType = @"recent";
        self.title = @"Recent Photos";
    }
    return self;
}

- (instancetype) initWithLocation:(NSString *)latitude longitude:(NSString *)longitude {
    self = [super init];
    if (self) {
        self.latitude = latitude;
        self.longitude = longitude;
        self.requestType = @"location";
        self.title = @"Around Your Location";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view = [[UIView alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    [self handleRequest];
}

- (void)viewWillAppear:(BOOL)animated {
    [_collectionView reloadData];
}

- (void)handleRequest {
    if ([self.requestType isEqualToString:@"tag"]) {
        NSArray *parameters = [[NSArray alloc] initWithObjects:@"method", @"flickr.photos.search", @"tags", self.tag, @"page", @"1", nil];
        [self requestPhotosWith: parameters];
    } else if ([self.requestType isEqualToString:@"recent"]) {
        NSArray *parameters = [[NSArray alloc] initWithObjects:@"method", @"flickr.photos.getRecent", @"page", @"1", nil];
        [self requestPhotosWith: parameters];
    } else if ([self.requestType isEqualToString:@"location"]) {
        NSArray *parameters = [[NSArray alloc] initWithObjects:@"method", @"flickr.photos.search", @"lat", self.latitude, @"lon", self.longitude, @"page", @"1", nil];
        [self requestPhotosWith:parameters];
    }
}

- (void)requestPhotosWith:(NSArray *)parameters {
    [FlickrRequest findPhotosWithParamaters:parameters completionHandler:^(FlickrData *data) {
        self.flickrData = data;
        [self configureCollectionView];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RequestFinished" object:nil];
        [self initializeGestures];
        [self alertIfCoordinatesAreZero];
        [self alertIfNoPhotoExists];
    }];
}

- (void)configureCollectionView {
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    layout.itemSize = CGSizeMake(90, 120);
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height + 20, self.view.frame.size.width, self.view.frame.size.height) collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    [_collectionView registerClass:[FlickrPhotoCollectionViewCell.self class] forCellWithReuseIdentifier:@"Cell"];
    _collectionView.backgroundColor = [UIColor whiteColor];
     self.automaticallyAdjustsScrollViewInsets = NO;
    [self.view addSubview:_collectionView];
}

- (void) initializeGestures {
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeLeft];
}

- (void)alertIfCoordinatesAreZero {
    if (self.latitude == 0 && self.longitude == 0 && [self.requestType isEqualToString:@"location"]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Result" message:@"Location not found" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)alertIfNoPhotoExists {
    if ([_flickrData.photos count] == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Result" message:@"No Result Found" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^
                    (UIAlertAction * action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_flickrData.photos count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:
                (NSIndexPath *)indexPath {
    FlickrPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell"
        forIndexPath:indexPath];
    NSURL *url = [[_flickrData.photos objectAtIndex:indexPath.row] url];
    [cell.imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    cell.imageView.userInteractionEnabled=YES;
    cell.imageView.contentMode = UIViewContentModeScaleToFill;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)
                            collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(collectionView.frame.size.width/2.5, collectionView.frame.size.width/2.5);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    CGRect rect = [keyWindow bounds];
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [keyWindow.layer renderInContext:context];
    UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    FlickrPhotoDetailViewController *viewController = [[FlickrPhotoDetailViewController alloc] initWithData:
        self.flickrData.photos index:(int)indexPath.row background:screenShot];
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell
                        forItemAtIndexPath:(NSIndexPath *)indexPath {
    int page = [self.flickrData.currentPage intValue];
    if (indexPath.row == [self.flickrData.photos count] - 1 && page < [_flickrData.totalPage intValue]) {
        if ([self.requestType isEqualToString:@"tag"]) {
            NSArray *parameters = [[NSArray alloc] initWithObjects:@"method", @"flickr.photos.search",
                @"tags", self.tag, @"page", [NSString stringWithFormat:@"%d", page + 1], nil];
            [FlickrRequest findPhotosWithParamaters:parameters completionHandler:^(FlickrData *data) {
                [self updateData:data];
            }];
        } else if ([self.requestType isEqualToString:@"recent"]) {
            NSArray *parameters = [[NSArray alloc] initWithObjects:@"method", @"flickr.photos.getRecent",
                @"page", [NSString stringWithFormat:@"%d", page + 1], nil];
            [FlickrRequest findPhotosWithParamaters:parameters completionHandler:^(FlickrData *data) {
                [self updateData:data];
            }];
        } else if ([self.requestType isEqualToString:@"location"]) {
            NSArray *parameters = [[NSArray alloc] initWithObjects:@"method", @"flickr.photos.search",
                @"lat", self.latitude, @"lon", self.longitude, @"page", [NSString stringWithFormat:@"%d",
                page + 1], nil];
            [FlickrRequest findPhotosWithParamaters:parameters completionHandler:^(FlickrData *data) {
                [self updateData:data];
            }];
        }
    }
}

- (void)updateData:(FlickrData *)data {
    [self.flickrData.photos addObjectsFromArray:data.photos];
    self.flickrData.currentPage = data.currentPage;
    self.flickrData.totalPage = data.totalPage;
    self.flickrData.perPage = data.perPage;
    [self.collectionView reloadData];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)
                collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    CGFloat leftRightInset = self.view.frame.size.width / 14.0f;
    return UIEdgeInsetsMake(0, leftRightInset, 0, leftRightInset);
}


-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
