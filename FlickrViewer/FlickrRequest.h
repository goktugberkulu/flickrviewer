//
//  FlickrRequest.h
//  FlickrViewer
//
//  Created by Goktug on 22/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#ifndef FlickrRequest_h
#define FlickrRequest_h

#import <Foundation/Foundation.h>
#import "FlickrData.h"

@interface FlickrRequest : NSObject

 + (void) findPhotosWithParamaters:(NSArray *)parameters completionHandler:(void (^)(FlickrData *data))callback;

@end

#endif /* FlickrRequest_h */
