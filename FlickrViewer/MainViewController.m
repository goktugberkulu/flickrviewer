//
//  MainViewController.m
//  FlickrViewer
//
//  Created by Goktug on 24/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import "MainViewController.h"
#import "FlickrRequest.h"
#import "ViewController.h"

@interface MainViewController ()

@property (strong, nonatomic) UIActivityIndicatorView *indicator;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.title = @"Flickr Viewer";
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicator.frame = CGRectMake(self.view.center.x - 100, self.view.center.y - 100, 200, 200);
    self.indicator.hidesWhenStopped = YES;
    self.indicator.hidden = YES;
    self.indicator.color = [UIColor blackColor];
    [self.view addSubview:self.indicator];
    [self configureSearchBar];
    [self configureButton:self.getRecentButton iconName:@"recent" xCoordinateRatio:1 selector:@selector(getRecentPhotos)];
    [self configureButton:self.locationBasedPhotoSearchButton iconName:@"location" xCoordinateRatio:3 selector:@selector(getLocationBasedPhotos)];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopIndicator) name:@"RequestFinished" object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    self.view.alpha = 1.0;
}

- (void) stopIndicator {
    [self.indicator stopAnimating];
}

- (void) configureSearchBar {
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height + 20, CGRectGetWidth(self.view.frame), 44)];
    self.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    self.searchBar.delegate = self;
    self.searchBar.placeholder = @"Enter a tag";
    [self.view addSubview:self.searchBar];
}

- (void) configureButton:(UIButton *)button iconName:(NSString *)iconName xCoordinateRatio:(int)ratio selector:(SEL)selector {
    button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake(ratio * self.view.frame.size.width/5, self.searchBar.frame.origin.y + self.searchBar.frame.size.height + 40, self.view.frame.size.width/5, self.view.frame.size.width/5);
    [button setBackgroundImage:[UIImage imageNamed:iconName] forState:UIControlStateNormal];
    [button addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

- (void)getRecentPhotos {
    self.indicator.hidden = NO;
    [self.indicator startAnimating];
    ViewController *viewController = [[ViewController alloc] initWithPage:@"1"];
    [UIView animateWithDuration:1.0 animations:^{
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [[self navigationController] pushViewController:viewController animated:YES];
       
    }];
}

- (void) getLocationBasedPhotos {
    self.indicator.hidden = NO;
    [self.indicator startAnimating];
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager requestWhenInUseAuthorization];
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    NSString *latitude = [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.longitude];
     ViewController *viewController = [[ViewController alloc] initWithLocation:latitude longitude:longitude];
    [UIView animateWithDuration:1.0 animations:^{
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [[self navigationController] pushViewController:viewController animated:YES];
        
    }];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar setText:@""];
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self.view endEditing:YES];
    self.indicator.hidden = NO;
    [self.indicator startAnimating];
    ViewController *viewController = [[ViewController alloc] initWithTag:searchBar.text];
    [UIView animateWithDuration:1.0 animations:^{
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [[self navigationController] pushViewController:viewController animated:YES];
        
    }];
}

@end
