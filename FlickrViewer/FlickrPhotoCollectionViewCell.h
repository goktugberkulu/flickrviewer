//
//  FlickrPhotoCollectionViewCell.h
//  FlickrViewer
//
//  Created by Goktug on 23/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrPhotoCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) UIImageView *imageView;

@end
