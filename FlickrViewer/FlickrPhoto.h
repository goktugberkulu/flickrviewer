//
//  FlickrPhoto.h
//  FlickrViewer
//
//  Created by Goktug on 23/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlickrPhoto : NSObject

@property (copy, nonatomic) NSString *title;
@property (strong, nonatomic) NSURL *url;

- (instancetype)initWithParamaters:(NSString *)title url:(NSURL *)url;

@end
