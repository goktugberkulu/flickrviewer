//
//  FlickrData.h
//  FlickrViewer
//
//  Created by Goktug on 23/06/16.
//  Copyright © 2016 Goktug. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FlickrPhoto.h"

@interface FlickrData : NSObject

@property (strong, nonatomic) NSMutableArray *photos;
@property (copy, nonatomic) NSNumber *currentPage;
@property (copy, nonatomic) NSNumber *totalPage;
@property (copy, nonatomic) NSNumber *perPage;

-(instancetype)initWithParameters:(NSMutableArray *)photos currentPage:(NSNumber *)currentPage totalPage:(NSNumber *)totalPage perPage:(NSNumber *)perPage;

@end
